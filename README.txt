Overview
================
This module provides integration with Sweetcapcha

Features
===========
Sweetcapcha integration with drupal 7

Dependencies : libraries(Download from https://www.drupal.org/project/libraries)

How to
================
Install the required modules
Copy sweetCaptcha package you downloaded : 
https://github.com/sweetcaptcha/sweetcaptcha-sdk-php
from the official site and place it at in your project: 
/sites/all/libraries/sweetCaptcha

===============================
Goto->http://sweetcaptcha.com
and register for App id, App key, App secret key.
====================================================
Goto->admin/config/sweetcaptcha for admin setting for including sweetcapcha
